$(function () {
    $('#add-to-cart').on('click', function () {
        $.ajax({
            method: 'POST',
            data: {
                'id': $(this).data('id'),
                '_token': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '/add-to-cart',
            success: function () {
                alert('Товар добавлен в корзину');
            }
        });
        return false;
    });

});
