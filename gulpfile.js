const elixir = require('laravel-elixir');

elixir(function (mix) {
    mix.sass('main.scss', 'public/styles/main.css')
        .webpack('main.js', 'public/scripts/main.js');
});