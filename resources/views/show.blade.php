@extends('layouts.base')

@section('title', 'Сапфировая коллекция - Комплект двухспальный')

@section('content')
    <div class="container">

        <ul class="bread-crumbs">
            <li class="bread-crumbs__item">
                <a href="{{ route('category') }}" class="bread-crumbs__link">Продукция Lama Gold</a>
            </li>
            <li class="bread-crumbs__item">
                <a href="{{ route('products', ['id' => $product->category->id ]) }}" class="bread-crumbs__link">{{ $product->category->name  }}</a>
            </li>
            <li class="bread-crumbs__item">
                <a href="{{ route('show', ['id' => $product->id ]) }}" class="bread-crumbs__link">{{ $product->name }}</a>
            </li>
        </ul>

        <div class="product-single">
            <div class="row between-xs">
                <div class="col-xs-12 col-sm-5">
                    <figure class="product-single__poster">
                        <img src="{{ $product->media->path }}" alt="{{ $product->media->alt }}">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h1 class="title">{{ $product->category->name }}</h1>
                    <h4>{{ $product->name }}</h4>
                    <ul>
                        <li>2 подушки;</li>
                        <li>одеяло;</li>
                        <li>наматрасник.</li>
                    </ul>

                    <div class="row product-single__data">
                        <div class="col-xs-12 col-sm-6">
                            <h2 class="product-single__value"><span>{{ $product->value }}</span> руб.</h2>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <a href="#" id="add-to-cart" data-id="{{ $product->id }}" class="btn add-to-cart">В корзину</a>
                        </div>
                    </div>
                    <div class="product-single__desc">
                        {!! $product->text  !!}

                        <a href="http://lamagold.by/presentation" target="_blank" class="more">Купить со скидкой на рекламной презентации</a>
                    </div>
                </div>
            </div>
            <div class="product-slider">
                <h2 class="title">Фото в интерьере</h2>

                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="/images/product.jpg" alt="">
                        </li>
                        <li>
                            <img src="/images/product.jpg" alt="">
                        </li>
                        <li>
                            <img src="/images/product.jpg" alt="">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection