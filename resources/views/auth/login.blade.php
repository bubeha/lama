@extends('layouts.base')

@section('content')
    <div class="container">
        <div class="row login">
            <div class="col-xs-12 col-sm-6">
                <figure class="login__poster">
                    <img src="./images/rectangle-2.jpg" alt="">
                </figure>
            </div>
            <div class="col-xs-12 col-sm-6">
                <h1 class="title">Авторизация</h1>
                <form class="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form__group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form__control" name="email" value="{{ old('email') }}"
                               placeholder="E-mail адрес" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form__control" name="password" placeholder="Пароль" required>
                        <a href="{{ route('password.request') }}" class="forgot-password">Забыли пароль?</a>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form__group text-center">
                        <button type="submit" class="btn btn-full">Войти на сайт</button>

                        <a href="{{ route('register') }}" class="register-link">Регистрация</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
