@extends('layouts.base')

@section('content')
    <div class="container">
        <div class="row login">
            <div class="col-xs-12 col-sm-6">
                <figure class="login__poster">
                    <img src="./images/rectangle-2.jpg" alt="">
                </figure>
            </div>
            <div class="col-xs-12 col-sm-6">
                <h1 class="title">Регистрация</h1>
                <form class="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="form__group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" class="form__control" name="name" value="{{ old('name') }}" placeholder="Имя"
                               required>

                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form__control" name="email" value="{{ old('email') }}"
                               placeholder="E-mail адрес" required>

                        @if ($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>

                    <div class="form__group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form__control" name="password" placeholder="Пароль" required>

                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>

                    <div class="form__group">
                        <input type="password" class="form__control" name="password_confirmation"
                               placeholder="Подтвердите пароль" required>
                    </div>

                    <div class="form__group">
                        <button type="submit" class="btn">Регистрация</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
