@extends('layouts.base')

@section('content')
    <div class="container">
        <div class="row login">
            <div class="col-xs-12 col-sm-6">
                <figure class="login__poster">
                    <img src="/images/rectangle-2.2.jpg" alt="">
                </figure>
            </div>
            <div class="col-xs-12 col-sm-6">
                <h1 class="title">Забыли пароль?</h1>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="form__group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form__control" name="email" value="{{ old('email') }}"
                               placeholder="E-mail адрес" required>

                        @if ($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>

                    <div class="form__group">
                        <button type="submit" class="btn btn-full btn-gray">Отправить ссылку на смену пароля</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
