@extends('layouts.base')

@section('title', 'Lamagold shop - Категории продукции')

@section('content')
    <div class="container">
        <h1 class="title">Продукция Lama Gold</h1>
        <div class="row">
            @foreach($category as $item)
                <div class="col-xs-12 col-sm-4">
                    <div class="product">
                        <a href="{{ route('products', ['id' => $item->id ]) }}" class="product__link">
                            <figure class="product__poster">
                                <img src="{{ $item->media->path }}" alt="{{ $item->media->alt }}">
                            </figure>
                            <h3 class="product__name">{{ $item->name }}</h3>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection