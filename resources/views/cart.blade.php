@extends('layouts.base')

@section('title', 'Корзина')

@section('content')
    <div class="container">
        <div class="cart">
            @if($cart)
                @foreach($cart as $item)
                    <div class="row middle-xs cart-item">
                        <div class="col-xs-12 col-sm-3">
                            <figure class="cart-item__poster">
                                <img src="./images/pillow-1738023-1920.png" alt="">
                            </figure>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <p class="cart-item__name">Пончо «Lama Gold»</p>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <span class="cart-item__value">189 руб.</span>
                            <span class="cart-item__number">1</span>
                            <a href="#" class="down"><i class="icon-arrow"></i></a>
                            <a href="#" class="up"><i class="icon-arrow"></i></a>
                        </div>
                        <div class="col-xs-12 col-sm-1 text-right">
                            <a class="cart-item__remove"><i class="icon-close"></i></a>
                        </div>
                    </div>
                @endforeach
                <div class="row end-xs">
                    <h2 class="all-value">276 руб.</h2>
                </div>
                <a href="{{ route('order') }}" class="btn btn-full"> Оформить заказ</a>
            @else
                <h2 class="sub-title">Корзина пуста</h2>
            @endif;
        </div>
    </div>
@endsection