@extends('layouts.base')

@section('title', 'Lamagold shop - Категории продукции')

@section('content')
    <div class="container">
        <h1 class="title">{{ $category->name }}</h1>
        <div class="row">
            @foreach($products as $product)
            <div class="col-xs-12 col-sm-4">
                <div class="product">
                    <a href="{{ route('show', ['id' => $product->id]) }}" class="product__link">
                        <figure class="product__poster">
                            <img src="{{ $product->media->path }}" alt="{{ $product->media->alt }}">
                        </figure>
                        <h3 class="product__name">{{ $product->name }}</h3>
                    </a>
                </div>
            </div>
            @endforeach;
        </div>
    </div>
@endsection