<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Lamagold shop')</title>

    <link rel="apple-touch-icon" href="/favicon.ico">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('styles/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('styles/main.css') }}">

</head>
<!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<header class="header header--main header--white">
    <div class="container">
        <div class="row middle-xs">
            <a href="{{ route('home') }}" class="logo">
                <img src="/images/logo-min.png" alt="">
            </a>
            <nav class="nav">
                <a href="{{ route('category') }}" class="nav__item">
                    Продукция
                </a>
                <a href="#" class="nav__item">
                    Lama Gold
                    &#9662;
                </a>
            </nav>
            <div class="user-panel">
                <a href="{{ route('cart') }}" class="user-panel__item">
                    <svg width="20" height="23" viewBox="0 0 20 23" xmlns="http://www.w3.org/2000/svg" class="icon">
                        <title>C22D8479-6D44-4767-9A4C-4E139D956794</title>
                        <path d="M19.872 21.704l-.884-4.794.46-11.32c.01-.246-.084-.486-.26-.664-.173-.178-.414-.278-.666-.278h-3.856v-.15c0-2.48-2.06-4.498-4.59-4.498s-4.59 2.018-4.59 4.497v.15H1.63c-.25 0-.493.102-.667.28-.175.177-.268.417-.258.663l.46 11.32-.908 4.92c-.05.264.025.536.2.74.176.208.436.327.71.327h17.827c.51 0 .926-.406.926-.907 0-.1-.017-.195-.048-.286zM7.338 4.497c0-1.48 1.228-2.683 2.738-2.683s2.74 1.203 2.74 2.683v.15H7.337v-.15zM2.275 21.083l.614-3.33L4.83 16.7l-1.84-.483-.397-9.756H17.56l-.39 9.635-1.786.47 1.845 1 .647 3.518H2.275z"
                              fill-rule="nonzero"></path>
                    </svg>
                </a>
                <a href="{{ route('login') }}" class="user-panel__item menu-btn">
                    Войти
                </a>
            </div>
        </div>
    </div>
</header>


@yield('content')

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X');
    ga('send', 'pageview');
</script>

<!-- Scripts -->
<script src="{{ asset('scripts/vendor.js') }}"></script>
<script src="{{ asset('scripts/main.js') }}"></script>
</body>
</html>