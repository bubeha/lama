@extends('layouts.base')

@section('title', ' Контакты')

@section('content')
    <div class="container">
        <div class="contact">
            <h1 class="title title--underline">Контакты</h1>

            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <ul class="contact__list">
                        <li>Телефон: +375 (29) 111-22-22 <br> 8 (0152) 11-22-33</li>
                        <li>Почта: <a href="mailto:lamagold@lamagold.by">lamagold@lamagold.by</a></li>
                        <li>Время работы: пн-пт: 8:30 - 17:00, <br> сб: 11:00 - 16:00, вс: выходной</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <form method="POST" class="form">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group">
                                    <input type="text" class="input" placeholder="Имя">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group">
                                    <input type="text" class="input" placeholder="+375 (29) ***-**-**">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group">
                                    <input type="email" class="input" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="form__group">
                            <textarea cols="30" rows="10" class="input" placeholder="Сообщение"></textarea>
                        </div>

                        <div class="form__group text-right">
                            <button type="submit" class="btn">Отправить</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection