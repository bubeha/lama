@extends('layouts.base')

@section('title', 'Оформить заказ')

@section('content')
    <div class="order">
        <div class="container">
            <div class="title">Оформить заказ</div>

            <form action="" class="form">
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                        <div class="form__group">
                            <input type="text" class="form__control" placeholder="Имя">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="form__group">
                            <input type="text" class="form__control" placeholder="+375 (29) ***-**-**">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="form__group">
                            <input type="email" class="form__control" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="order-value">
                            <p>Сумма заказа</p>
                            <h2 class="value">327 руб.</h2>
                        </div>
                    </div>
                </div>
                <div class="form__group">
                    <div class="col-xs-12">
                        <h2 class="order-name">Способ доставки</h2>
                        <a href="#" class="btn btn--no-bg">Курьером</a>
                    </div>
                </div>
                <div class="form__group">
                    <div class="col-xs-12">
                        <h2 class="order-name">Оплата</h2>
                        <a href="#" class="btn btn--no-bg">Наличными при получении товара</a>
                    </div>
                </div>

                <div class="form__group">
                    <textarea cols="30" rows="10" class="form__control" placeholder="Комментарий к заказу"></textarea>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <a href="#" class="btn">Отправить заказ</a>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <a href="{{ route('cart') }}" class="back"><i class="icon-arrow"></i>  Вернуться в корзину</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection