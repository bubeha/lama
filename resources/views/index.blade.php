@extends('layouts.index')

@section('title', 'Lamagold shop - Главная')

@section('content')
    <div id="fullpage" class="fullpage">
        <section class="section">
            <div class="container">
                <img src="/images/hero-img.jpg" alt="Золотая коллекция Ламаголд" class="section__poster">
                <div class="block">
                    <h3 class="block__type">Комплект постельного белья</h3>
                    <h2 class="title">Золотая коллекция</h2>
                    <ul class="block__desc">
                        <li>4 размера</li>
                        <li>100% шерсть мериноса</li>
                        <li>Не впитывает запахи</li>
                        <li>Не вызывает аллергию</li>
                    </ul>

                    <a href="#" class="block__more btn btn--no-bg">
                        Подробнее
                    </a>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <img src="/images/hero-img.jpg" alt="Золотая коллекция Ламаголд" class="section__poster">
                <div class="block">
                    <h3 class="block__type">Комплект постельного белья</h3>
                    <h2 class="title">Золотая коллекция</h2>
                    <ul class="block__desc">
                        <li>4 размера</li>
                        <li>100% шерсть мериноса</li>
                        <li>Не впитывает запахи</li>
                        <li>Не вызывает аллергию</li>
                    </ul>

                    <a href="#" class="block__more btn btn--no-bg">
                        Подробнее
                    </a>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <img src="/images/hero-img.jpg" alt="Золотая коллекция Ламаголд" class="section__poster">
                <div class="block">
                    <h3 class="block__type">Комплект постельного белья</h3>
                    <h2 class="title">Золотая коллекция</h2>
                    <ul class="block__desc">
                        <li>4 размера</li>
                        <li>100% шерсть мериноса</li>
                        <li>Не впитывает запахи</li>
                        <li>Не вызывает аллергию</li>
                    </ul>

                    <a href="#" class="block__more btn btn--no-bg">
                        Подробнее
                    </a>
                </div>
            </div>
        </section>
    </div>
    <ul id="fullpage-menu" class="fullpage-menu">
        <li data-menuanchor="first" class="fullpage-menu__item">
            <a href="#first" class="fullpage-menu__link">
                <span>Золото</span>
            </a>
        </li>
        <li data-menuanchor="second" class="fullpage-menu__item">
            <a href="#second" class="fullpage-menu__link">
                <span>Золото</span>
            </a>
        </li>
        <li data-menuanchor="last" class="fullpage-menu__item">
            <a href="#last" class="fullpage-menu__link">
                <span>Золото</span>
            </a>
        </li>
    </ul>
@endsection