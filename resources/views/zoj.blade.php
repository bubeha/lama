@extends('layouts.base')

@section('title', 'Здоровый образ жизни')

@section('content')
    <div class="container">
        <div class="single">
            <h1 class="title">Здоровый образ жизни</h1>

            <div class="video">
                <img src="/images/rectangle-7.jpg" alt="">
            </div>
        </div>
    </div>
@endsection