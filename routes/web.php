<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@index')->name('home');
Route::get('/category', 'DefaultController@category')->name('category');
Route::get('/category/{id}', 'DefaultController@products')->name('products');
Route::get('/show/{name}', 'DefaultController@show')->name('show');
Route::get('/contact', 'DefaultController@contact')->name('contact');
Route::get('/cart', 'DefaultController@cart')->name('cart');
Route::get('/order', 'DefaultController@order')->name('order');

Route::get('/zoj', function() {
    return view('zoj');
})->name('zoj');

Route::get('/prevention', function() {
    return view('prevention');
})->name('prevention');

Route::get('/comfort', function() {
    return view('comfort');
})->name('comfort');

Route::get('/about', function() {
    return view('about');
})->name('about');

Route::get('/delivery', function() {
    return view('delivery');
})->name('delivery');

Auth::routes();

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/add-to-cart', 'CartController@add');
