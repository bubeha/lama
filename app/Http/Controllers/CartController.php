<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{

    function add(Request $request)
    {

        $productID = $request->request->get('id');
        $cart = $request->session()->get('cart');
        $newCart = [];
        $inCart = false;

        if (!$cart) {
            $cart = ['id' => $productID, 'count' => 1];
        } else {
            foreach ($cart as $item) {
                if ($item->id == $productID) {
                    $item->count += 1;
                    $inCart = true;
                }
            }

            if (!$inCart) {
                $cart[] = ['id' => $productID, 'count' => 1];
            }
        }


        var_dump($cart);


        return response()
            ->json(['status' => 'ok']);
    }

}
