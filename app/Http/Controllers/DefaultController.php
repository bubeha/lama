<?php

namespace App\Http\Controllers;


use App\Category;
use App\Product;
use Illuminate\Http\Request;

class DefaultController extends Controller
{

    public function index()
    {

        return view('index');
    }

    public function category()
    {
        $category = Category::all();

        return view('category', ['category' => $category]);
    }

    public function products($id)
    {
        $category = Category::find($id);
        $products = Product::where('category_id', $id)->get();

        return view('products', ['category' => $category, 'products' => $products]);
    }

    public function show($id)
    {
        $product = Product::find($id);

        return view('show', ['product' => $product]);
    }

    public function contact()
    {
        return view('contact');
    }

    public function cart(Request $request)
    {
        $cart = $request->session()->get('cart');


        return view('cart', ['cart' => $cart]);
    }

    public function order()
    {
        return view('order');
    }


}